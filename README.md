# Test Project#

### Author: ###
Oleg Derecha

### Technologies/Platfroms: ###
* .NET
* Angular
* MS SQL Server

### ScreenShots: ###
Login screen:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6fSHlpdzBCR0x4VFU)

Receptions:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6fbXc0cHUyVUNUTGM)

Reception details:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6fZ0VkQnZ5eTNlS0k)

Questions:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6fcXhvOHM4SXBidlk)

Question details:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6fR2NQSEhuQlZwMHM)

Comments:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6fQ09tTnlrM1hvb00)

Comment details:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6fSnc0UmdtX1hSZGc)

Users:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6feHEwTEYwdzM4Nm8)

User Details:
![Screen](https://drive.google.com/uc?export=download&id=0B0FQwE_B3v6fTFVMMHZzeGx2V1E)