﻿using AutoMapper;
using DataLayer.DomainModels.Entities;
using BusinessLayer.ServiceModels;

namespace PresentationLayer.Web.Mappings {
    public class GeneralMappings : Profile {
        protected override void Configure() {

            Mapper.CreateMap<Question, QuestionModel>().ReverseMap();
            Mapper.CreateMap<Reception, ReceptionModel>().ReverseMap();
            Mapper.CreateMap<Comment, CommentModel>().ReverseMap();
            
            Mapper.CreateMap<User, UserModel>().ReverseMap()
                .ForMember(e => e.Password, opt => opt.Ignore())
                .ForMember(e => e.PasswordSalt, opt => opt.Ignore());
        }
    }
}