﻿using System.Web.Http;
using BusinessLayer.ServiceModels;
using BusinessLayer.Services;

namespace PresentationLayer.Web.Controllers
{
    public class NavigationController : ApiController
    {
        private readonly NavigationService _navigationService = new NavigationService();

        [HttpGet]
        public NavigationModel GetCount()
        {
            return _navigationService.GetCount();
        }

       
    }
}