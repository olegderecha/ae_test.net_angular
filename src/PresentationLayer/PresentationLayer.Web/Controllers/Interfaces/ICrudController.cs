﻿using BusinessLayer.ServiceModels.Base;

namespace PresentationLayer.Web.Controllers.Interfaces
{
    interface ICrudController<T> where T : class, IBaseModel
    {
        T GetById(int id);
        bool Insert(T entityModel);
        bool Update(T entityModel);
        bool Delete(T entityModel);
    }
}