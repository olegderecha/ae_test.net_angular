﻿using System.Web.Mvc;

namespace PresentationLayer.Web.Controllers
{
    public class WebController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
