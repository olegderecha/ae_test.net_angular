﻿using System.Web.Http;
using BusinessLayer.ServiceModels;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.Services;
using PresentationLayer.Web.Controllers.Base;
using PresentationLayer.Web.Models;

namespace PresentationLayer.Web.Controllers
{
    public class ReceptionController : BaseCrudController<ReceptionModel>
    {
        private readonly ReceptionService _receptionService = new ReceptionService();

        [HttpPost]
        public ReceptionsModel GetAll(PaginationRequest request)
        {
            return _receptionService.GetAll(request.Page, request.Count, request.SortBy ?? "CreatedAt", request.SortOrder);
        }
        
        [HttpPost]
        public bool MarkAsRead(ModelRequest request)
        {
            return _receptionService.MarkAsRead(request.Id);
        }
    }
}