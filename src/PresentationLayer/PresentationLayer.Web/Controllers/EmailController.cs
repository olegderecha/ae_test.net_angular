﻿using System.Web.Http;
using BusinessLayer.Services;
using PresentationLayer.Web.Models;

namespace PresentationLayer.Web.Controllers {
    public class EmailController : ApiController{
        private readonly EmailService _mailService = new EmailService();

        [HttpPost]
        public bool SendAnswer(EmailRequest request)
        {
            return _mailService.SendAnswerEmail(request.QuestionId, request.MessageText);
        }

    }
}
