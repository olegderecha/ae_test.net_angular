﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using BusinessLayer.ServiceModels;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.Services;
using PresentationLayer.Web.Controllers.Base;
using PresentationLayer.Web.Models;

namespace PresentationLayer.Web.Controllers
{
    public class CommentController : BaseCrudController<CommentModel>
    {
        private readonly CommentService _commentService = new CommentService();

        [HttpPost]
        public CommentsModel GetAll(PaginationRequest request)
        {
            return _commentService.GetAll(request.Page, request.Count, request.SortBy ?? "CreatedAt", request.SortOrder);
        }

        [HttpPost]
        public bool MarkAsRead(ModelRequest request)
        {
            return _commentService.MarkAsRead(request.Id);
        }

        [HttpPost]
        public bool Publish(ModelRequest request)
        {
            return _commentService.Publish(request.Id);
        }
    }
}