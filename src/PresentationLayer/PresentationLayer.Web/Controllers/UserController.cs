﻿using System.Web.Http;
using BusinessLayer.ServiceModels;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.Services;
using PresentationLayer.Web.Controllers.Base;
using PresentationLayer.Web.Models;

namespace PresentationLayer.Web.Controllers
{
    public class UserController : BaseCrudController<UserModel>
    {
        private readonly UserService _userService = new UserService();

        [HttpPost]
        public UsersModel GetAll(PaginationRequest request)
        {
            return _userService.GetAll(request.Page, request.Count, request.SortBy ?? "Login", request.SortOrder);
        }

        [HttpPost]
        public override bool Insert(UserModel userModel)
        {
            return _userService.Insert(userModel);
        }

        [HttpPost]
        public override bool Update(UserModel userModel)
        {
            return _userService.Update(userModel);
        }
    }
}