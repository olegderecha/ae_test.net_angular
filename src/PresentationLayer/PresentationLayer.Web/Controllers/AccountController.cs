﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using BusinessLayer.Services;
using DataLayer.DomainModels.Entities;

namespace PresentationLayer.Web.Controllers {
    public class AccountController : ApiController{
        private readonly AccountService _accountService = new AccountService();

        // ========== Forms ==========
        [HttpPost]
        public HttpResponseMessage LogIn(User user) {
            if (_accountService.AreCredentialsCorrect(user)) {
                FormsAuthentication.SetAuthCookie(user.Login, createPersistentCookie: false);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "");
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage LogOut() {
            FormsAuthentication.SignOut();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public string IsUserLogged() {
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated){
                return HttpContext.Current.User.Identity.Name;
            }
            return null;
        }
    }
}
