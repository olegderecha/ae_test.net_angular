﻿using System.Web.Http;
using BusinessLayer.ServiceModels.Base;
using BusinessLayer.Services.Base;

namespace PresentationLayer.Web.Controllers.Base
{
    public class BaseCrudController<T> : ApiController where T : class, IBaseModel {
        private readonly BaseCrudService<T> _baseCrudService = new BaseCrudService<T>();

        [HttpGet]
        public virtual T GetById(int id) {
            return _baseCrudService.GetById(id);
        }
        [HttpPost]
        public virtual bool Insert(T entityModel) {
            return _baseCrudService.Insert(entityModel);
        }

        [HttpPost]
        public virtual bool Update(T entityModel) {
            return _baseCrudService.Update(entityModel);
        }

        [HttpPost]
        public virtual bool Delete(T entityModel) {
            return _baseCrudService.Delete(entityModel.Id);
        }

        [HttpPost]
        public bool Hide(T entityModel) {
            return _baseCrudService.Hide(entityModel.Id);
        }

        [HttpPost]
        public bool Show(T entityModel) {
            return _baseCrudService.Show(entityModel.Id);
        }
    }
}