﻿using System.Web.Http;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.ServiceModels;
using BusinessLayer.Services;
using PresentationLayer.Web.Controllers.Base;
using PresentationLayer.Web.Models;

namespace PresentationLayer.Web.Controllers
{
    public class QuestionController : BaseCrudController<QuestionModel>
    {
        private readonly QuestionService _questionService = new QuestionService();

        [HttpPost]
        public QuestionsModel GetAll(PaginationRequest request)
        {
            return _questionService.GetAll(request.Page, request.Count, request.SortBy ?? "CreatedAt", request.SortOrder);
        }

        [HttpPost]
        public bool MarkAsRead(ModelRequest request)
        {
            return _questionService.MarkAsRead(request.Id);
        }
        
    }
}