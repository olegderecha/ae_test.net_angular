﻿angular.module('common').
factory('Config', ['$http', function($http) {
    var self = {};

    /* === Properties === */
    self.IsAdminLogged = false;

    self.BaseUrl = function() {
        return window.Hospital.initialConfiguration.baseUrl;
    }();
    
    self.BaseUrlFull = function() {
        return window.location.origin + window.Hospital.initialConfiguration.baseUrl;
    }();

    self.ApiUrl = function() {
        return self.BaseUrl + "api/";
    }();

    self.TransferServiceUrl = function() {
        return self.BaseUrl;
    }();

    self.UploadUrl = function() {
        return self.BaseUrl + "api/Transfer/UploadVideo";
    }();

    self.AbsoluteSessionUrl = function(sessionId) {
        return self.BaseUrl;
    };


    // Azure Storage
    self.StorageUrl = function() {
        return "http://cinamaker.blob.core.windows.net/";
    }();

    self.VideoStorageUrl = function() {
        return self.StorageUrl + "video/";
    }();

    self.AudiioStorageUrl = function() {
        return self.StorageUrl + "audio/";
    }();

    self.ThumbnailStorageUrl = function() {
        return self.StorageUrl + "thumbnail/";
    }();



    //// Google Docs
    //self.StorageUrl = function() {
    //    //return "https://docs.google.com/uc?export=download&id=";
    //    return "https://googledrive.com/host/";
    //}();

    //self.VideoStorageUrl = function() {
    //    return self.StorageUrl;
    //}();

    //self.AudiioStorageUrl = function() {
    //    return self.StorageUrl;
    //}();

    //self.ThumbnailStorageUrl = function() {
    //    return self.StorageUrl;
    //}();


    self.getQualityList = function(videoQualityList) {
        var qualityList = {
            'highres': { param: 'highres', display: 'Maximum' },
            'hd1080': { param: 'hd1080', display: '1080p' },
            'hd720': { param: 'hd720', display: '720p' },
            'large': { param: 'large', display: '480p' },
            'medium': { param: 'medium', display: '360p' },
            'small': { param: 'small', display: '240p' },
            'tiny': { param: 'tiny', display: '160p' },
            'default': { param: 'default', display: 'Default' },
            'auto': { param: 'auto', display: 'Auto' }
        };

        var resultList = [];
        _.each(videoQualityList, function(quality) {
            resultList.push(qualityList[quality]);
        }, this);
        
        return resultList;
    };

    return self;
}]);