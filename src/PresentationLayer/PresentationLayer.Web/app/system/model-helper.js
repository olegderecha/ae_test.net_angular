angular.module('common').
factory('ModelHelper', ['$state', '$window', '$location', 'Config',
function($state, $window, $location, Config) {
    var self = {};
    var enDashCode = "&#8211;"; // �
    var emDashCode = "&#8212;"; // �
    
    self.fillEmpyFields = function(object) {
        for (var key in object) {
            if (!object[key] || object[key] == "") {
                object[key] = emDashCode;
            }
        }
        return object;
    };
    
    return self;
}]);