angular.module('common').
factory('GoogleAPI', ['Config', function(Config) {
    var self = {};
    
    var config = {
        client_id: '357184518995-6fvjm7m688vk797oml3dcmh96vrp9g0r.apps.googleusercontent.com', // web-application
        scope: 'https://www.googleapis.com/auth/drive', // Google Drive
        immediate: false
    };

    self.token = null;

    self.request = function() {
        var fileId = "0BwcNTVg5GwOYeTBzal9jR0NDb1E";
        self.getFileInfo(fileId);
    };

    self.checkAuth = function() {
        //gapi.auth.authorize({ client_id: clientId, scope: scopes, immediate: false }, self.handleAuthResult);
        gapi.auth.authorize(config, function() {
            self.token = gapi.auth.getToken().access_token;
            console.log(self.token);
        });
    };
    
    self.getFileInfo = function(fileId) {
        var request = gapi.client.request({
            'path': '/drive/v2/files/' + fileId,
            'method': 'GET',
            'params': {
                'access_token': self.token
            }
        });
        
        request.execute(function(resp) {
            if (!resp.error) {
                console.log('Title: ' + resp.title);
                console.log('Description: ' + resp.description);
                console.log('MIME type: ' + resp.mimeType);
            } else if (resp.error.code == 401) {
                // Access token might have expired.
                self.checkAuth();
            } else {
                console.log('An error occured: ' + resp.error.message);
            }
        });
    };
    
    self.uploadFile = function(fileId) {
        var request = gapi.client.request({
            'path': '/upload/drive/v2/files',
            'method': 'POST',
            'params': {
                'access_token': self.token
            }
        });

        request.execute(function(resp) {
            if (!resp.error) {
                console.log('Title: ' + resp.title);
                console.log('Description: ' + resp.description);
                console.log('MIME type: ' + resp.mimeType);
            } else if (resp.error.code == 401) {
                // Access token might have expired.
                self.checkAuth();
            } else {
                console.log('An error occured: ' + resp.error.message);
            }
        });
    };
    


    self.printFile = function() {
        var fileId = "0B0FQwE_B3v6fZEhnRGRUeU1Bbnc";
        var request = gapi.client.drive.files.get({
            'fileId': fileId
        });
        request.execute(function(resp) {
            if (!resp.error) {
                console.log('Title: ' + resp.title);
                console.log('Description: ' + resp.description);
                console.log('MIME type: ' + resp.mimeType);
            } else if (resp.error.code == 401) {
                // Access token might have expired.
                checkAuth();
            } else {
                console.log('An error occured: ' + resp.error.message);
            }
        });
    };

    self.handleAuthResult = function(authResult) {
        gapi.client.load('drive', 'v2');
        self.printFile();

        return;
        var authorizeButton = document.getElementById('authorize-button');
        if (authResult && !authResult.error) {
            authorizeButton.style.visibility = 'hidden';
            makeApiCall();
        } else {
            authorizeButton.style.visibility = '';
            authorizeButton.onclick = handleAuthClick;
        }
    };

    self.handleAuthClick = function (event) {
        gapi.auth.authorize({ client_id: clientId, scope: scopes, immediate: false }, handleAuthResult);
        return false;
    };
    
    self.makeApiCall = function () {
        gapi.client.load('plus', 'v1').then(function() {
            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });
            request.then(function(resp) {
                var heading = document.createElement('h4');
                var image = document.createElement('img');
                image.src = resp.result.image.url;
                heading.appendChild(image);
                heading.appendChild(document.createTextNode(resp.result.displayName));

                document.getElementById('content').appendChild(heading);
            }, function(reason) {
                console.log('Error: ' + reason.result.error.message);
            });
        });
    };
     
    return self;
}]);