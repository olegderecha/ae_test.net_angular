angular.module('common').
factory('PaginationHelper', ['Config', function (Config) {
    var self = {};

    self.getDefault = function () {
        return {
            page: 1,
            count: 25
        };
    };

    return self;
}]);