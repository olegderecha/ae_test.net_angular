angular.module('common').
factory('Helper', ['Config', function(Config) {
    var self = {};
    
    self.transformVideoUrls = function(videos) {
        _.each(videos, function(obj) {
            //obj.FullUrl = Config.TransferServiceUrl + obj.Url + '?i=' + Math.random();
            obj.FullUrl = Config.TransferServiceUrl + obj.Url;
            obj.FullSoundTrackUrl = Config.TransferServiceUrl + obj.SoundTrackUrl;
        }, this);
    };
    
    self.transformUrls = function(videos) {
        _.each(videos, function(obj) {
            obj.FullUrl = Config.VideoStorageUrl + obj.Url;
            obj.FullSoundTrackUrl = Config.AudiioStorageUrl + obj.SoundTrackUrl;
            obj.FullThumbnailUrl = Config.ThumbnailStorageUrl + obj.ThumbnailUrl;
        }, this);
    };
    
    self.transformSessionUrls = function(sessions) {
        _.each(sessions, function(obj) {
            obj.AbsoluteUrl = Config.AbsoluteSessionUrl(obj.Id);
        }, this);
    };
    

    self.makeAutoHideable = function(element) {
        $(document).mouseup(function(e) {
            var container = element;

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.addClass('hidden');
            }
        });
    };
    

    self.getTimeString = function(time) {
        var minutesString;
        var secondsString;
        var hoursString;

        var minutes = Math.floor(time / 60);
        if (minutes < 10) {
            minutesString = "0" + minutes;
        } else {
            minutesString = minutes;
        }

        var seconds = Math.floor(time - minutes * 60);
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = seconds;
        }

        var hours = Math.floor(time / 3600);
        if (hours < 10) {
            hoursString = "0" + hours;
        } else {
            hoursString = hours;
        }
        
        if (hours > 0) {
            return hoursString + ":" + minutesString + ":" + secondsString;
        } else {
            return minutesString + ":" + secondsString;
        }
    };
    
    self.generateGUID = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    };
     
    return self;
}]);