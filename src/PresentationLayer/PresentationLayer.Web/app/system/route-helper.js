angular.module('common').
factory('RouteHelper', ['$state', '$window', '$location', 'BrowserHelper', 'Config',
function($state, $window, $location, BrowserHelper, Config) {
    var self = {};
    
    self.redirectToSessionList = function() {
        $state.go('default', { notify: false });
        setTimeout(function() {
            $window.location.reload();
        }, 100);
    };

    self.redirectWithReload = function(stateName, id) {
        id ? $state.go(stateName, { id: id }, { notify: false }) :
             $state.go(stateName, { notify: false });
        setTimeout(function() {
            $window.location.reload();
        }, 500);
        
    };
    
    self.redirectToDetails = function(storageTypeName, id) {
        if (BrowserHelper.IsMobile) {
            switch (storageTypeName) {
                case "Azure":
                    $window.location = Config.BaseUrlFull + 'Mobile/#/m-video-details-dl/' + id;
                    //$window.location.reload();
                    break;
                case "YouTube":
                    $window.location = Config.BaseUrlFull + 'Mobile/#/m-video-details-yt/' + id;
                    //$window.location.reload();
                    break;
            }

            
        } else {
            switch (storageTypeName) {
                case "Azure":
                    self.redirectWithReload('video-details-dl', id);
                    break;
                case "YouTube":
                    self.redirectWithReload('video-details-yt', id);
                    break;
            }
        }
    };
    
    self.redirectToDetailsWithReload = function(storageTypeName, id) {
        if (BrowserHelper.IsMobile) {
            switch (storageTypeName) {
                case "Azure":
                    $window.location = Config.BaseUrlFull + 'Mobile/#/m-video-details-dl/' + id;
                    //$location.path(Config.BaseUrlFull + 'Mobile/#/m-video-details-dl/' + id, false);
                    $window.location.reload();
                    break;
                case "YouTube":
                    //$location.path(Config.BaseUrlFull + 'Mobile/#/m-video-details-yt/' + id, false);
                    $window.location = Config.BaseUrlFull + 'Mobile/#/m-video-details-yt/' + id;
                    $window.location.reload();
                    break;
            }


        } else {
            switch (storageTypeName) {
                case "Azure":
                    self.redirectWithReload('video-details-dl', id);
                    break;
                case "YouTube":
                    self.redirectWithReload('video-details-yt', id);
                    break;
            }
        }
    };
    
    return self;
}]);