app.run(function($rootScope, $route, $location) {
    //Bind the `$locationChangeSuccess` event on the rootScope, so that we dont need to 
    //bind in induvidual controllers.
    $rootScope.$on('$locationChangeSuccess', function() {
        $rootScope.actualLocation = $location.path();
    });

    $rootScope.$watch(function() { return $location.path() }, function(newLocation, oldLocation) {
        if ($rootScope.actualLocation === newLocation) {
            // back button detected
            $rootScope.$broadcast('navigation:backButtonClicked', newLocation);
            console.log('here?');
        }
    });
});