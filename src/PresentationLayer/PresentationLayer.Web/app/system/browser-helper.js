angular.module('common').
factory('BrowserHelper', ['$state', '$window', function($state, $window) {
    var self = {};
    
    self.IsMobile = function() {
        return !!$.browser.mobile;
    }();
      
    self.IsDesktop = function() {
        return !$.browser.mobile;
    }();
    
    self.IsSafari = function() {
        if ($.browser.safari && $.browser.desktop && $.browser.mac) {
            return true;
        }
        return false;
    }();
    
    self.IsMozilla = function() {
        if ($.browser.mozilla && $.browser.desktop) {
            return true;
        }
        return false;
    }();
    
    self.checkAllowBrowser = function() {
        // iOS
        if (!!$.browser.mobile &&
            ($.browser.ipad || $.browser.iphone || $.browser.ipod)) {
            $state.go('ios-download-redirect', { notify: true });
            return false;
        }
        return true;
    };
    
    self.getBrowserInfo = function() {
        var info = "";
        info = info + "$.browser.msie" + ": " + $.browser.msie;
        info = info + "\n$.browser.webkit" + ": " + $.browser.webkit;
        info = info + "\n$.browser.chrome" + ": " + $.browser.chrome;
        info = info + "\n$.browser.opera" + ": " + $.browser.opera;
        info = info + "\n$.browser.mozilla" + ": " + $.browser.mozilla;
        info = info + "\n$.browser.version" + ": " + $.browser.version;
        info = info + "\n$.browser.versionNumber" + ": " + $.browser.versionNumber;
        info = info + "\n$.browser.android" + ": " + $.browser.android;
        info = info + "\n$.browser.blackberry" + ": " + $.browser.blackberry;
        info = info + "\n$.browser.cros" + ": " + $.browser.cros;
        info = info + "\n$.browser.ipad" + ": " + $.browser.ipad;
        info = info + "\n$.browser.iphone" + ": " + $.browser.iphone;
        info = info + "\n$.browser.ipod" + ": " + $.browser.ipod;
        info = info + "\n$.browser.kindle" + ": " + $.browser.kindle;
        info = info + "\n$.browser.linux" + ": " + $.browser.linux;
        info = info + "\n$.browser.mac" + ": " + $.browser.mac;
        info = info + "\n$.browser.playbook" + ": " + $.browser.playbook;
        info = info + "\n$.browser.silk" + ": " + $.browser.silk;
        info = info + "\n$.browser.win" + ": " + $.browser.win;
        info = info + "\n$.browser[windows phone]" + ": " + $.browser["windows phone"];
        info = info + "\n$.browser.desktop" + ": " + $.browser.desktop;
        info = info + "\n$.browser.mobile" + ": " + $.browser.mobile;
        info = info + "\n$.browser.platform" + ": " + $.browser.platform;
        alert(info);
        return info;
    };
    
    
    return self;
}]);