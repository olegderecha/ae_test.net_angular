angular.module('directives').
    directive('domElementLoaded', function () {
        return {
            link: function($scope, elem, attrs, ctrl) {
                var functionName = attrs.domElementLoaded.toString();
                functionName = functionName.replace('(', '');
                functionName = functionName.replace(')', '');
                $scope[functionName]();
            }
        };
    });