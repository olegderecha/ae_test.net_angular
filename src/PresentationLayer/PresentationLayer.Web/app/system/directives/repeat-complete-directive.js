﻿angular.module('directives').
    directive('repeatComplete', ['$rootScope', function($rootScope) {
        return function($scope, element, attrs) {
            if ($scope.$last) setTimeout(function() {
                $rootScope.$broadcast('system:repeatComplete', element, attrs);
            }, 1);
    };
}]);