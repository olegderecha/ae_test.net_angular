angular.module('directives').
    directive('domLoaded', function () {
        return {
            link: function($scope, elem, attrs, ctrl) {

                var functionName = attrs.domLoaded;
                $scope[functionName]();

                //funcRunner($scope, attrs.repeatHelloWorld);
                //$scope.doSomething2();
                //$scope.[attrs.repeatHelloWorld]();


                /*
                var hello = function() {
                    for (var i = 0; i < attrs.repeatHelloWorld; i++) {
                        console.log("Hello world!");
                    }
                };
                hello();*/
            },
            
            funcRunner: function($scope, functionName) {
                $scope[functionName]();
            }
        };
    });