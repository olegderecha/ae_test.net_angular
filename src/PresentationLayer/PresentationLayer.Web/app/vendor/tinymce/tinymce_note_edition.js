﻿/// <reference path="jquery.tinymce.min.js" />
/// <reference path="jquery.tinymce.min.js" />
TINYMCENoteEdition = (function() {
    function editor() { };

    var executionLocationFolder;

    editor.create = function(options) {
        editor._setPaths();
        editor._loadEditorJSFile();

        if (!editor.getEditorById(options.id)) {
            // ===== create editor instance =====
            tinyMCE.init({
                selector: "#" + options.id,
                mode: "specific_textareas",

                height: options.height,

                theme_url: this.themeLocationPath,
                skin_url: this.skinLocationFolder,
                content_css: this.customCSSLocationPath,
                fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",

                toolbar1: "bold italic underline | fontselect | fontsizeselect",
                menubar: false,
                statusbar: false,
                autosave_ask_before_unload: false,
                setup: function(editorInstance) {

                    editorInstance.on('init', function() {
                        editor._editorLoadedHandler(options.content);
                    });

                    editorInstance.on('keyup', function() {
                        editor._contentChangeHandler(options.content);
                    });
                }
            });
            
        } else {
            // ===== set content if editor already exists =====
            editor.setContentById(options.id, options.content);
        }

        return tinyMCE;
    };

    editor.isExists = function() {
        return tinyMCE.activeEditor ? true : false;
    };
    
    editor.getContent = function() {
        var content = null;
        if (tinyMCE.activeEditor) {
            content = tinyMCE.activeEditor.getContent();
        }
        return content;
    };
    
    editor.getContentLength = function() {
        var contentLength = null;
        if (tinyMCE.activeEditor) {
            contentLength = $(tinyMCE.activeEditor.getContent()).text().length;
        }
        return contentLength;
    };
    
    editor.getActiveEditor = function() {
        return tinyMCE.activeEditor;
    };

    editor.getEditorById = function(id) {
        return tinyMCE.get(id);
    };
    
    editor.setToolbarToDefault = function() {
        editor.getActiveEditor().theme.panel.find('toolbar * button').active(false);
    };
    
    editor.setContentById = function(editorId, content) {
        if (content) {
            editor.getEditorById(editorId).setContent(content);
        } else {
            editor.getEditorById(editorId).setContent('');
        }
        editor.setToolbarToDefault();
    };
    
    editor.setContent = function(content) {
        if (content) {
            editor.getActiveEditor().setContent(content);
        } else {
            editor.getActiveEditor().setContent('');
        }
        editor.setToolbarToDefault();
    };
     
    editor.disposeById = function(id) {
        if (typeof tinyMCE != "undefined") {
            var currentEditor = editor.getEditorById(id);
            if (currentEditor) {
                $(currentEditor.targetElm.parentElement).remove();
            }
        }
    };
    
    // =========== Event Handlers =============
    editor._editorLoadedHandler = function(content) {
        editor.setContent(content);
        //Backbone.Events.trigger('TINYMCENoteEdition.Loaded');
    };
    
    editor._contentChangeHandler = function() {      
        //Backbone.Events.trigger('TINYMCENoteEdition.ContentChanged');
    };
    
    // =========== Paths =============
    editor._setPaths = function() {
        this.editorJSFilePath = "app/vendor/tinymce/tinymce.min.js";
        this.themeLocationPath = "app/vendor/tinymce/themes/note_edition/theme.js";
        this.skinLocationFolder = "app/vendor/tinymce/themes/note_edition";
        this.customCSSLocationPath = "app/vendor/tinymce/themes/note_edition/custom.css";
    };
    
    editor._loadEditorJSFile = function() {
        if (typeof tinyMCE == "undefined") {
            $.ajax({
                async: false,
                url: this.editorJSFilePath,
                dataType: "script"
            });
        }
    };

    return editor;
})();