﻿Notification = (function() {
    var self = {};

    self.settings = {
        stayTime: 2000,				// time in miliseconds before the item has to disappear
        sticky: false				// should the toast item sticky or not?
    };

    var types = {
        notice: "notice",
        success: "success",
        warning: "warning",
        error: "error"
    };

    self.showMessage = function(type, message) {
        if (!type || !types[type]) {
            throw "ShowMessage: Type is incorrect";
        }
        if (!message) {
            throw "ShowMessage: Text is required";
        }

        self.settings.type = type;
        self.settings.text = message;
        self.settings.sticky = (type == 'error');

        $().toastmessage('showToast', self.settings);
    };

    return self;
})();