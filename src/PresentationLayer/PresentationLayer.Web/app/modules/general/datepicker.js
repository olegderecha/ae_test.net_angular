JQueryDatetimePicker = (function () {
    var self = {};

    self.init = function () {
        $.datetimepicker.setLocale('ru');

        $('#dtpDateReception').datetimepicker({
            timepicker: false,
            format: 'd.m.Y'
        });

        $('#dtpTimeReception').datetimepicker({
            datepicker: false,
            format: 'H:i',
            defaultTime: '08:00',
            allowTimes: [
              '08:00',
              '09:00',
              '10:00',
              '11:00',
              '12:00',
              '13:00',
              '14:00',
              '15:00',
              '16:00',
              '17:00'
            ]
        });
    }

    return self;
})();