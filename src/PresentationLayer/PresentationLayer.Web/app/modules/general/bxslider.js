﻿BxSlider = (function () {
    var self = {};

    self.init = function() {
        $('.bxslider').bxSlider({
            auto: true
        });
    }

    return self;
})();