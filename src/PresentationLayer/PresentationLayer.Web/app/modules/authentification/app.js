﻿var app = angular.module('app', [
    'controllers',
    'services',
    'factories',
    'directives',
    'ngRoute',
    'ui.router',
    'blockUI'
]);

angular.module('common', []);
angular.module('directives', []);
angular.module('factories', ['common']);
angular.module('services', ['factories']);
angular.module('controllers', ['services', 'factories', 'directives']);