angular.module('controllers').
controller('AuthenticationController', ['AuthenticationService', 'Helper', 'RouteHelper', 'BrowserHelper', '$scope', '$rootScope', '$window',
    function(authenticationService, Helper, RouteHelper, BrowserHelper, $scope, $rootScope, $window) {
        var self = this;

        $scope.model = {};
        $scope.currentUser = {
            login: null,
            password: null,
            isLogged: false
        };

        $scope.logIn = function () {
            authenticationService.logIn($scope.model);
        };

        $scope.logOut = function () {
            authenticationService.logOut();
        };

        $scope.isUserLogged = function () {
            authenticationService.isUserLogged($scope);
        };
      
        self.init = function () {
            $scope.isUserLogged();
            self.initElements();
            self.bindEvents();
        };
        
        self.initElements = function() {
            self.$userLogin = $('#user-login');
            self.$userPassword = $('#user-password');
        };
        
        self.bindEvents = function() {
            self.$userLogin.on('keyup', function(e) {
                if (e.keyCode == 13) {
                    $scope.logIn();
                }
            });
            
            self.$userPassword.on('keyup', function(e) {
                if (e.keyCode == 13) {
                    $scope.logIn();
                }
            });
        };
        
        self.unBindEvents = function() {
            self.$userLogin.off('keyup');
            self.$userPassword.off('keyup');
        };
        
        // ===== Events =====
        $rootScope.$on('authentication:error', function(event, video) {
            $scope.currentUser.password = null;
        });

        $scope.$watch('$viewContentLoaded', function() {
            self.init();
        });
    }]);