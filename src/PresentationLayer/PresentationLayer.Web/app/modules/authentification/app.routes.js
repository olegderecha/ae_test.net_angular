﻿app.config(['$routeProvider', '$stateProvider', '$urlRouterProvider', '$sceDelegateProvider',
function($routeProvider, $stateProvider, $urlRouterProvider, $sceDelegateProvider) {
    var baseUrl = window.location.origin;
    var baseAdminUrl = baseUrl + "/app/modules/admin/";   
    $urlRouterProvider.otherwise('/');
}]);