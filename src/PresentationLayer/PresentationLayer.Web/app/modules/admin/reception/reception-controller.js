﻿angular.module('controllers').
controller('ReceptionController', ['$scope', '$rootScope', '$stateParams', '$location', 'PaginationHelper', 'Config', 'ReceptionService', 'ModelHelper',
function ($scope, $rootScope, $stateParams, $location, paginationHelper, Config, receptionService, modelHelper) {
    var self = this;

    // ===== Pagination ===== //
    $scope.init = paginationHelper.getDefault();
    self.paginationParams = {};

    $scope.BaseUrl = Config.BaseUrl;
    $scope.model = {};
    self.currentHideItem = {};
    
    self.init = function () {};
    self.initDeleteConfirmation = function () {
        $('[data-toggle="confirmation-popout"]').confirmation({
            popout: true,
            href: "#/receptions/",
            onConfirm: function () {
                self.currentHideItem.target.confirmation('hide');
                $scope.hide(self.currentHideItem.item);
            }
        });
    };

    $scope.showDetails = function (item) {
        $scope.model = item;
        if (!$scope.model.isRead) {
            $scope.markAsRead($scope.model);
        }
        $("#reception-details").modal("show");
    };

    $scope.reloadCallback = function () { };

    $scope.getAll = function (params, paramsObj) {
        if (!paramsObj) return {};
        self.paginationParams = paramsObj;
        return receptionService.getAll(paramsObj).then(function (response) {
            return {
                "header": [
                    { "key": "fullName", "name": "Full Name" },
                    { "key": "date", "name": "Reception Date" },
                    { "key": "phone", "name": "Phone" },
                    { "key": "email", "name": "E-mail" },
                    { "key": "createdAt", "name": "Create" },
                    { "key": "buttons", "name": "" }
                ],
                "rows": response.data.receptions,
                "pagination": response.data.receptions,
                "sort-by": "title",
                "sort-order": "asc"
            };
        });
    };

    $scope.onHide = function ($event, item) {
        self.currentHideItem = {
            item: item,
            target: $($event.target)
        };
    };

    $scope.hide = function (item) {
        receptionService.hide(item.id);
    };

    $scope.markAsRead = function (model) {
        receptionService.markAsRead(model);
    };


    // ===== Events ===== //
    $scope.$on("$viewContentLoaded", function () {
        self.init();
    });

    $rootScope.$on("reception:delete", function (event, args) {
        $scope.questions = args;
    });

    $rootScope.$on("reception:hide", function (event, args) {
        $scope.reloadCallback();
    });

    $rootScope.$on("reception:markAsRead", function (event, args) {
        $scope.model.isRead = true;
    });

    $rootScope.$on("system:repeatComplete", function (event, args) {
        self.initDeleteConfirmation();
    });
	
}]);