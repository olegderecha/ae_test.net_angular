﻿angular.module('controllers').
controller('QuestionController', ['QuestionService', '$scope', '$rootScope', '$stateParams', 'PaginationHelper', 'Config', 'CrudService',
function (questionService, $scope, $rootScope, $stateParams, paginationHelper, Config, crudService) {
    var self = this;
    var questionCrudService = new crudService("question", "question");

    // ===== Pagination ===== //
    $scope.init = paginationHelper.getDefault();
    self.paginationParams = {};

    $scope.BaseUrl = Config.BaseUrl;
    $scope.model = {};
    self.currentHideItem = {};

    self.init = function () { };
    self.initDeleteConfirmation = function () {
        $('[data-toggle="confirmation-popout"]').confirmation({
            popout: true,
            href: "#/questions/",
            onConfirm: function () {
                self.currentHideItem.target.confirmation('hide');
                $scope.hide(self.currentHideItem.item);
            }
        });
    };

    $scope.showDetails = function (item) {
        $scope.model = item;
        if (!$scope.model.isRead) {
            $scope.markAsRead($scope.model);
        }
        TINYMCENoteEdition.create({
            id: "question",
            height: 300
        });
        $("#question-details").modal("show");
    };
    
    // ======== data ========
    $scope.reloadCallback = function () { };

    $scope.getAll = function (params, paramsObj) {
        if (!paramsObj && paramsObj.page > 0) return {};
        self.paginationParams = paramsObj;
        return questionService.getAll(paramsObj).then(function (response) {
            return {
                "header": [
                    { "key": "title", "name": "Title" },
                    { "key": "createdAt", "name": "Created" },
                    { "key": "isAnswered", "name": "" },
                    { "key": "buttons", "name": "" }
                ],
                "rows": response.data.questions,
                "pagination": response.data.pagination,
                "sort-by": "title",
                "sort-order": "asc"
            };
        });
    };

    $scope.onHide = function ($event, item) {
        self.currentHideItem = {
            item: item,
            target: $($event.target)
        };
    };

    $scope.hide = function (item) {
        questionService.hide(item.id);
    };

    $scope.sendAnswer = function () {
        questionService.sendAnswer({
            questionId: $scope.model.id,
            messageText: TINYMCENoteEdition.getContent()
        });
    };

    $scope.markAsRead = function (model) {
        questionService.markAsRead(model);
    };

    // question ask
    $scope.submitQuestion = function () {
        questionService.insert($scope.model);
    };

    $scope.questionAskModalClose = function (model) {
        $rootScope.$broadcast("modal:questionAskClose");
    };


    // ===== Events ===== //
    $scope.$on("$viewContentLoaded", function () {
        self.init();
    });

    $rootScope.$on("question:changed", function (event, args) {
        $scope.questions = args;
    });

    $rootScope.$on("question:hide", function (event, args) {
        $scope.reloadCallback();
    });

    $rootScope.$on("question:sendAnswer", function (event, args) {
        $scope.model.answer = null;
        $scope.model.isAnswered = true;
        $("#question-details").modal("hide");
    });

    $rootScope.$on("question:markAsRead", function (event, args) {
        $scope.model.isRead = true;
    });


    $rootScope.$on("question:shownPopup", function (event, args) {
        $scope.model = {};
        $scope.error = {};
    });

    $rootScope.$on("question:errors", function (event, args) {
        $scope.error = args;
        console.log($scope.error);
    });

    $rootScope.$on("question:insert", function (event, args) {
        $scope.model = {};
        $scope.error = {};
        $scope.questionAskModalClose();
    });


    $rootScope.$on("system:repeatComplete", function (event, args) {
        self.initDeleteConfirmation();
    });
}]);