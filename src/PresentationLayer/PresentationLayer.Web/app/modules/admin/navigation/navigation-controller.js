﻿angular.module('controllers').
controller('NavigationController', ['$scope', '$rootScope', '$timeout', 'NavigationService', function ($scope, $rootScope, $timeout, navigationService) {
    var self = this;

    $scope.count = {
        repectionsCount: 0,
        questionsCount: 0,
        commentsCount: 0
    }

    self.init = function () {
        self.getCount();
    };

    self.getCount = function () {
        navigationService.getCount();
    };

    // ===== Events ===== //
    $scope.$watch("$viewContentLoaded", function () {
        self.init();
    });

    $rootScope.$on("navigation:getCount", function (event, args) {
        $scope.count = args;
    });
    $rootScope.$on("navigation:refresh", function (event, args) {
        self.init();
        console.log('nav-refresh');
    });


}]);