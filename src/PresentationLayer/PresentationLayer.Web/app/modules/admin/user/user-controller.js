﻿angular.module('controllers').
controller('UserController', ['$scope', '$rootScope', '$stateParams', '$location', 'PaginationHelper', 'Config', 'UserService', 'ModelHelper',
function ($scope, $rootScope, $stateParams, $location, paginationHelper, Config, userService, modelHelper) {
    var self = this;

    // ===== Pagination ===== //
    $scope.init = paginationHelper.getDefault();
    self.paginationParams = {};

    $scope.BaseUrl = Config.BaseUrl;
    $scope.model = {};
    self.currentHideItem = {};
    
    self.init = function () {
        $("#phone").mask("(000) 000-00-00");
    };
    self.initDeleteConfirmation = function () {
        $('[data-toggle="confirmation-popout"]').confirmation({
            popout: true,
            href: "#/users/",
            onConfirm: function () {
                self.currentHideItem.target.confirmation('hide');
                $scope.hide(self.currentHideItem.item);
            }
        });
    };

    $scope.reloadCallback = function () { };

    $scope.getAll = function (params, paramsObj) {
        if (!paramsObj) return {};
        self.paginationParams = paramsObj;
        return userService.getAll(paramsObj).then(function (response) {
            return {
                "header": [
                    { "key": "login", "name": "Login" },
                    { "key": "firstName", "name": "First Name" },
                    { "key": "lastName", "name": "Last Name" },
                    { "key": "email", "name": "E-mail" },
                    { "key": "phone", "name": "Phone" },
                    { "key": "createdAt", "name": "Created" },
                    { "key": "buttons", "name": "" }
                ],
                "rows": response.data.users,
                "pagination": response.data.users,
                "sort-by": "title",
                "sort-order": "asc"
            };
        });
    };

    $scope.editUser = function (item) {
        if (item) {
            $scope.model = item;
            $scope.model.password = "******";
            $scope.model.passwordConfirm = "******";
        } else {
            $scope.model = {};
        }

        $("#user-details").modal("show");
    };

    $scope.saveUser = function () {
        if ($scope.model.id) {
            userService.updateUser($scope.model);
        } else {
            userService.inserUser($scope.model);
        }
    };

    $scope.onHide = function ($event, item) {
        self.currentHideItem = {
            item: item,
            target: $($event.target)
        };
    };

    $scope.hide = function (item) {
        userService.hide(item);
    };

    // ===== Events ===== //
    $scope.$on("$viewContentLoaded", function () {
        self.init();
    });

    $rootScope.$on("user:errors", function (event, args) {
        $scope.error = args;
    });

    $rootScope.$on("user:insert", function (event, args) {
        $("#user-details").modal("hide");
        $scope.reloadCallback();
    });

    $rootScope.$on("user:update", function (event, args) {
        $("#user-details").modal("hide");
        $scope.reloadCallback();
    });

    $rootScope.$on("user:delete", function (event, args) {
        $scope.questions = args;
    });

    $rootScope.$on("user:hide", function (event, args) {
        $scope.reloadCallback();
    });

    $rootScope.$on("system:repeatComplete", function (event, args) {
        self.initDeleteConfirmation();
    });
	
}]);