﻿angular.module('controllers').
controller('CommentController', ['$scope', '$rootScope', '$stateParams', '$location', 'PaginationHelper', 'Config', 'CommentService', 'ModelHelper',
function ($scope, $rootScope, $stateParams, $location, paginationHelper, Config, commentService, modelHelper) {
    var self = this;

    // ===== Pagination ===== //
    $scope.init = paginationHelper.getDefault();
    self.paginationParams = {};

    $scope.BaseUrl = Config.BaseUrl;
    $scope.model = {};
    self.currentHideItem = {};
    
    self.init = function () {};
    self.initDeleteConfirmation = function () {
        $('[data-toggle="confirmation-popout"]').confirmation({
            popout: true,
            href: "#/comments/",
            onConfirm: function () {
                self.currentHideItem.target.confirmation('hide');
                $scope.hide(self.currentHideItem.item);
            }
        });
    };

    $scope.showDetails = function (item) {
        $scope.model = item;
        if (!$scope.model.isRead) {
            $scope.markAsRead($scope.model);
        }
        $("#comment-details").modal("show");
    };

    $scope.reloadCallback = function () { };

    $scope.getAll = function (params, paramsObj) {
        if (!paramsObj) return {};
        self.paginationParams = paramsObj;
        return commentService.getAll(paramsObj).then(function (response) {
            return {
                "header": [
                    { "key": "firstName", "name": "Name" },
                    { "key": "email", "name": "E-mail" },
                    { "key": "createdAt", "name": "Created" },
                    { "key": "isPublished", "name": "" },
                    { "key": "buttons", "name": "" }
                ],
                "rows": response.data.comments,
                "pagination": response.data.comments,
                "sort-by": "title",
                "sort-order": "asc"
            };
        });
    };

    $scope.onHide = function ($event, item) {
        self.currentHideItem = {
            item: item,
            target: $($event.target)
        };
    };

    $scope.hide = function (item) {
        commentService.hide(item.id);
    };

    $scope.markAsRead = function (model) {
        commentService.markAsRead(model);
    };

    $scope.publish = function () {
        commentService.publish($scope.model);
    };

    // ===== Events ===== //
    $scope.$on("$viewContentLoaded", function () {
        self.init();
    });

    $rootScope.$on("comment:delete", function (event, args) {
        $scope.questions = args;
    });

    $rootScope.$on("comment:hide", function (event, args) {
        $scope.reloadCallback();
    });

    $rootScope.$on("comment:markAsRead", function (event, args) {
        $scope.model.isRead = true;
    });

    $rootScope.$on("comment:publish", function (event, args) {
        $scope.model.isPublished = true;
        $("#comment-details").modal("hide");
    });

    $rootScope.$on("system:repeatComplete", function (event, args) {
        self.initDeleteConfirmation();
    });
	
}]);