﻿app.config(['$routeProvider', '$stateProvider', '$urlRouterProvider', '$sceDelegateProvider',
function($routeProvider, $stateProvider, $urlRouterProvider, $sceDelegateProvider) {

    var baseUrl = window.location.origin;
    var baseAdminUrl = baseUrl + "/app/modules/admin/";

    $stateProvider
        .state('default1', {
            url: '',
            views: {
                'main': {
                    templateUrl: baseAdminUrl + 'reception/views/receptions.html',
                    controller: 'ReceptionController'
                }
            }
        })
        .state('default2', {
            url: '/',
            views: {
                'main': {
                    templateUrl: baseAdminUrl + 'reception/views/receptions.html',
                    controller: 'ReceptionController'
                }
            }
        })

        // Login
        .state('login', {
            url: '/login/',
            views: {
                'main': {
                    templateUrl: baseAdminUrl + 'authentication/views/login.html',
                    controller: 'AuthenticationController'
                }
            }
        })

        // Reception
        .state('receptions', {
            url: '/receptions/',
            views: {
                'main': {
                    templateUrl: baseAdminUrl + 'reception/views/receptions.html',
                    controller: 'ReceptionController'
                }
            }
        })
        .state('reception-get', {
            url: '/reception/get/:Id',
            views: {
                'menu': {
                    templateUrl: baseAdminUrl + 'menu/views/menu.html',
                    controller: 'MenuController'
                },
                'main': {
                    templateUrl: baseAdminUrl + 'question/views/question.html',
                    controller: 'QuestionController'
                }
            }
        })


        // Comments
        .state('comments', {
            url: '/comments/',
            views: {
                'main': {
                    templateUrl: baseAdminUrl + 'comment/views/comments.html',
                    controller: 'CommentController'
                }
            }
        })
        .state('comment-get', {
            url: '/comment/get/:Id',
            views: {
                'menu': {
                    templateUrl: baseAdminUrl + 'menu/views/menu.html',
                    controller: 'MenuController'
                },
                'main': {
                    templateUrl: baseAdminUrl + 'comment/views/comment.html',
                    controller: 'CommentController'
                }
            }
        })

        // Questions
        .state('questions', {
            url: '/questions/',
            views: {
                'main': {
                    templateUrl: baseAdminUrl + 'question/views/questions.html',
                    controller: 'QuestionController'
                }
            }
        })

        // Users
        .state('users', {
            url: '/users/',
            views: {
                'main': {
                    templateUrl: baseAdminUrl + 'user/views/users.html',
                    controller: 'UserController'
                }
            }
        })
        ;

    
    $urlRouterProvider.otherwise('/');
}]);