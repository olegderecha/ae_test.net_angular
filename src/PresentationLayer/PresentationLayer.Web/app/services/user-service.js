﻿angular.module('services').
service('UserService', ['$rootScope', 'Config', 'Helper', 'CrudService', 'UserDataFactory',
function ($rootScope, Config, Helper, crudService, userDataFactory) {
    var self = this;
    self.modelName = "user";
    var userCrudService = new crudService(self.modelName, self.modelName);

    self.model = {};

    self.getAll = function (paramsObj) {
        return userDataFactory.getAll(paramsObj);
    };

    self.inserUser = function (model) {
        if (!self.isValid(model)) return;
        userCrudService.insert(model).success(function (response) {
            Notification.showMessage("success", "Пользователь создан.");
            $rootScope.$broadcast(self.modelName + ":insert", {});
        })
    };

    self.updateUser = function (model) {
        if (!self.isValid(model)) return;
        userCrudService.update(model).success(function (response) {
            Notification.showMessage("success", "Пользователь обновлен.");
            $rootScope.$broadcast(self.modelName + ":update", {});
        })
    };

    self.hide = function (model) {
        if (model.login == "admin") {
            Notification.showMessage("warning", "Пользователя <b>admin</b> является системным. Удалить невозможно!");
            return;
        }
        userCrudService.hide(model.id).success(function (response) {
            Notification.showMessage("success", "Запись удалена.");
            $rootScope.$broadcast(self.modelName + ":hide");
        })
    };

    // validation
    self.isValid = function (model) {
        var error = {};

        // login
        if (!model.login) {
            error.login = "Введите login";
        } else {
            if (!self.isLoginValid(model.login)) error.login = "Логин должен содержать только латинские буквы";
            if (model.login.length < 4) error.login = "Логин должен содержать не менее 4-х символов";
            if (model.login.length > 50) error.login = "Логин должен содержать не более 50-ти символов";
        }

        // password
        if (model.password != "******") {
            if (!model.password) {
                error.password = "Введите пароль";
            } else {
                if (model.password.length < 8) error.password = "Пароль должен содержать не менее 8-ми символов";
                if (model.password.length > 20) error.password = "Пароль должен содержать не более 20-ти символов";
            }
        }
        
        // passwordConfirm
        if (!model.passwordConfirm) {
            error.passwordConfirm = "Подтвердите пароль";
        } else {
            if (model.password != model.passwordConfirm) error.passwordConfirm = "Введенные пароли не совпадают";
        }

        // first name
        if (!model.firstName) {
            error.firstName = "Введите имя";
        } else {
            if (model.firstName.length < 2) error.firstName = "Имя должно содержать не менее 2-х символов";
            if (model.firstName.length > 50) error.firstName = "Имя должно содержать не более 50-ти символов";
        }

        // last name
        if (!model.lastName) {
            error.lastName = "Введите фамилию";
        } else {
            if (model.lastName.length < 2) error.lastName = "Фамилия должна содержать не менее 2-х символов";
            if (model.lastName.length > 50) error.lastName = "Фамилия должна содержать не более 50-ти символов";
        }

        // email
        if (!model.email) {
            error.email = "Введите e-mail";
        } else {
            if (!self.isEmailValid(model.email)) error.email = "E-mail имеет неверный формат";
            if (model.email.length > 100) error.email = "E-mail должен содержать не более 100 символов";
        }

        // phone
        if (!model.phone) {
            error.phone = "Введите телефон";
        } else {
            var phone = model.phone.replace("-", "").replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
            if (phone.length < 10) error.phone = "Телефон имеет неверный формат";
        }
        
        $rootScope.$broadcast("user:errors", error);
        return $.isEmptyObject(error);
    };

    self.isEmailValid = function (email) {
        var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return regex.test(email);
    };

    self.isLoginValid = function (login) {
        var regex = /^[A-Za-z0-9_-]{3,16}$/;
        return regex.test(login);
    };

}]);