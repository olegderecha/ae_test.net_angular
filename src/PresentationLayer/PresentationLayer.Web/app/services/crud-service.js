﻿angular.module('factories').
factory('CrudService', ['CrudDataFactory', '$window', '$location', '$rootScope', 'DataService',
function(CrudDataFactory, $window, $location, $rootScope, dataService) {

    var instance = function(modelName) {
        var self = this;
        self.modelName = modelName;
        var crudDataFactory = new CrudDataFactory(self.modelName);
        
        this.getCollection = function(paramId) {
            return crudDataFactory.getCollection(paramId);
        };

        this.getById = function(id) {
            return crudDataFactory.getById(id);
        };

        this.insert = function(model) {
            return crudDataFactory.insert(model);
        };

        this.update = function(model) {
            return crudDataFactory.update(model);
        };

        this.delete = function (model) {
            return crudDataFactory.delete(model);
        };

        this.show = function (model) {
            return crudDataFactory.show(model);
        };

        this.hide = function (model) {
            return crudDataFactory.hide(model);
        };

        return this;
    };
    
    return instance;
}]);