﻿angular.module('services').
service('ReceptionService', ['$rootScope', 'Config', 'Helper', 'CrudService', 'ReceptionDataFactory',
function ($rootScope, Config, Helper, crudService, receptionDataFactory) {
    var self = this;
    self.modelName = "reception";
    var receptionCrudService = new crudService(self.modelName, self.modelName);

    self.model = {};

    self.getAll = function (paramsObj) {
        return receptionDataFactory.getAll(paramsObj);
    };

    self.insert = function (model) {
        if (!self.isValid(model)) return;
        receptionCrudService.insert(model, self.insertSuccess)
            .success(function(response) {
                self.insertSuccess();
            });
    };

    self.insertSuccess = function () {
        swal({
            title: "Your request accepted.",
            text: "Wait for our call.",
            type: "info",
            confirmButtonText: "OK",
            allowOutsideClick: true,
            animation: false
        });
        $rootScope.$broadcast(self.modelName + ":insert", {});
    };

    self.hide = function (id) {
        receptionCrudService.hide(id)
            .success(function(response) {
                Notification.showMessage("success", "Reception was deleted.");
                $rootScope.$broadcast(self.modelName + ":hide");
                $rootScope.$broadcast("navigation:refresh");
            });
    };

    self.markAsRead = function (id) {
        receptionDataFactory.markAsRead(id)
            .success(function(response) {
                $rootScope.$broadcast(self.modelName + ":markAsRead");
                $rootScope.$broadcast("navigation:refresh");
            });
    };

    // validation
    self.isValid = function (model) {
        var error = {};

        // title
        if (!model.fullName) {
            error.fullName = "Name is required.";
        } else {
            if (model.fullName.length < 2) error.fullName = "Name should contain not less than 2 chars.";
        }

        // email
        if (model.email) {
            if (!self.isEmailValid(model.email)) error.email = "E-mail has invalid format.";
            if (model.email.length > 100) error.email = "Email should not contain more than 100 chars.";
        }

        // phone
        if (!model.phone) {
            error.phone = "Phone is required.";
        }

        // date
        model.date = $("#dtpDateReception").val();
        if (!model.date || model.date === "") {
            error.date = "Enter a date.";
        }

        // time
        model.time = $("#dtpTimeReception").val();
        if (!model.time || model.time === "") {
            error.time = "Enter a time.";
        }

        model.date = self.getFullDate(model.date, model.time);

        // description
        if (!model.description) {
            error.description = "Enter a comment.";
        } else {
            if (model.description.length < 2) error.description = "Comment should contain not less than 2 chars.";
        }
        
        $rootScope.$broadcast("reception:errors", error);
        return $.isEmptyObject(error);
    };

    self.isEmailValid = function (email) {
        var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return regex.test(email);
    };

    self.isDateValid = function (date) {
        return moment(date, "DD.MM.YYYY").isValid();
    };

    self.getFullDate = function (date, time) {
        if (self.isDateValid(date) && time !== "") {
            var hours = $("#dtpTimeReception").val().substring(0, 2);
            var minutes = $("#dtpTimeReception").val().substring(2, 2);
            return moment(date, "DD.MM.YYYY").startOf("day").hour(hours).minute(minutes).format('YYYY-MM-DD HH:mm');
        }
    };

}]);