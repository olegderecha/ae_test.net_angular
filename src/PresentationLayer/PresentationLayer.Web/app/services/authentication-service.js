﻿angular.module('services').
    service('AuthenticationService', ['AuthenticationDataFactory', 'Config', '$window', '$rootScope',
        function(authenticationDataFactory, Config, $window, $rootScope) {
    var self = this;

    self.logIn = function(user) {
        authenticationDataFactory.logIn(user).
            success(function() {
                $window.location.href = "/Admin";
            }).
            error(function() {
                Notification.showMessage('warning', 'Login or passowrd are wrong.');
                $rootScope.$broadcast('authentication:error');
            }.bind(this));
    };

    self.logOut = function () {
        authenticationDataFactory.logOut().
            success(function() {
                $window.location.href = "/Login";
            }).
            error(function() {
            }.bind(this));
    };

    self.isUserLogged = function($scope) {
        authenticationDataFactory.isUserLogged().
            success(function(response) {
                if (response) {
                    $scope.currentUser.isLogged = true;
                    $scope.currentUser.name = response;
                    Config.IsAdminLogged = true;
                } else {
                    $scope.currentUser.isLogged = false;
                    Config.IsAdminLogged = false;
                }
                $rootScope.$broadcast('config:isUserLogged', response);
            }).
            error(function() {
                
            }.bind(this));
    };

}]);