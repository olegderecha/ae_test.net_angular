﻿angular.module('services').
service('QuestionService', ['$rootScope', 'Config', 'Helper', 'CrudService', 'QuestionDataFactory',
function ($rootScope, Config, Helper, crudService, questionDataFactory) {
    var self = this;
    self.modelName = "question";
    var questionCrudService = new crudService(self.modelName, self.modelName);

    self.model = {};

    self.getAll = function (paramsObj) {
        return questionDataFactory.getAll(paramsObj);
    };

    self.insert = function (model) {
        if (!self.isValid(model)) return;
        questionCrudService.insert(model).success(function(response) {
            Notification.showMessage("success", "Question was sent.");
            $rootScope.$broadcast(self.modelName + ":insert", {});
        });
    };

    self.hide = function(id) {
        questionCrudService.hide(id).success(function(response) {
            Notification.showMessage("success", "Question was deleted.");
            $rootScope.$broadcast(self.modelName + ":hide");
            $rootScope.$broadcast("navigation:refresh");
        });
    };

    self.sendAnswer = function (model) {
        if (model.messageText === "") {
            Notification.showMessage("success", "Answer can not be empty.");
            return;
        }
        questionDataFactory.sendAnswer(model).success(function(response) {
            Notification.showMessage("success", "Answer sent.");
            $rootScope.$broadcast(self.modelName + ":sendAnswer");
        });
    };

    self.markAsRead = function (id) {
        questionDataFactory.markAsRead(id).success(function(response) {
            $rootScope.$broadcast(self.modelName + ":markAsRead");
            $rootScope.$broadcast("navigation:refresh");
        });
    };

    // validation
    self.isValid = function (model) {
        var error = {};

        // title
        if (!model.title) {
            error.title = "Topic is required.";
        } else {
            if (model.title.length < 2) error.title = "Topic should contain not less than 2 chars.";
        }

        // description
        if (!model.description) {
            error.description = "Descriptions is required.";
        } else {
            if (model.description.length < 2) error.description = "Description should contain not less than 2 chars.";
        }

        // first name
        if (!model.firstName) {
            error.firstName = "Name is required.";
        } else {
            if (model.firstName.length < 2) error.firstName = "Name should contain not less than 2 chars.";
            if (model.firstName.length > 50) error.firstName = "Name should not contain more than 50 chars.";
        }

        // last name
        if (model.lastName) {
            if (model.lastName.length < 2) error.lastName = "Last name should contain not less than 2 chars.";
            if (model.lastName.length > 50) error.lastName = "Last name should not contain more than 50 chars.";
        }

        // email and phone 
        if (!model.email && !model.phone) {
            error.email = "E-mail or phone are required.";
            error.phone = "E-mail or phone are required.";
        }

        // email
        if (model.email) {
            if (!self.validateEmail(model.email)) error.email = "E-mail has invalid format.";
            if (model.email.length > 100) error.email = "Email should not contain more than 100 chars.";
        }

        // phone
        if (model.phone) {
            var phone = model.phone.replace("-", "");
            if (phone < 10) model.phone = "Phone has invalid format.";
        }

        $rootScope.$broadcast("question:errors", error);
        return $.isEmptyObject(error);
    };

    self.validateEmail = function (email) {
        var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return regex.test(email);
    };

}]);