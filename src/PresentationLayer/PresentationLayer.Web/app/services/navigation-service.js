﻿angular.module('services').
service('NavigationService', ['$rootScope', 'Config', 'Helper', 'CrudService', 'NavigationDataFactory',
function ($rootScope, Config, Helper, crudService, navigationDataFactory) {
    var self = this;
    self.modelName = 'navigation';

    self.getCount = function () {
        navigationDataFactory.getCount().success(function (response) {
            $rootScope.$broadcast(self.modelName + ":getCount", response);
        })
    };
   
}]);