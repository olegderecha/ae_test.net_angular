angular.module('factories').
factory('NavigationDataFactory', ['$http', 'Config', function($http, Config) {   
    var self = {};

    self.getCount = function () {
        return $http.get(Config.ApiUrl + 'Navigation/GetCount');
    };

    return self;
}]);