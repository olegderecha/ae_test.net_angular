angular.module('factories').
factory('CommentDataFactory', ['$http', 'Config', function($http, Config) {   
    var self = {};

    self.getAll = function (paramsObj) {
        return $http.post(Config.ApiUrl + 'Comment/GetAll', paramsObj);
    };

    self.markAsRead = function (model) {
        return $http.post(Config.ApiUrl + 'Comment/MarkAsRead', model);
    };

    self.publish = function (model) {
        return $http.post(Config.ApiUrl + 'Comment/Publish', model);
    };

    return self;
}]);