angular.module('factories').
factory('CrudDataFactory', ['$http', 'Config', function($http, Config) {
   
    var instance = function(modelName) {
        var self = this;
        self.modelName = modelName;

        this.getCollection = function(paramId) {
            if (paramId) {
                return $http.get(Config.ApiUrl + self.modelName + '/GetCollectionByParameterId/' + paramId);
            } else {
                return $http.get(Config.ApiUrl + self.modelName + '/GetAll/');
            }
        };

        this.getById = function(id) {
            return $http.get(Config.ApiUrl + self.modelName + '/GetById/' + id);
        };

        this.insert = function(model) {
            return $http.post(Config.ApiUrl + self.modelName + '/Insert/', model);
        };

        this.update = function(model) {
            return $http.post(Config.ApiUrl + self.modelName + '/Update/', model);
        };

        this.delete = function(id) {
            return $http.post(Config.ApiUrl + self.modelName + '/Delete/', { id: id });
        };

        this.hide = function (id) {
            return $http.post(Config.ApiUrl + self.modelName + '/Hide/', { id: id });
        };

        this.show = function (id) {
            return $http.post(Config.ApiUrl + self.modelName + '/Show/', { id: id });
        };

    };

    return instance;
}]);