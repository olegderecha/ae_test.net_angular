angular.module('factories').
factory('QuestionDataFactory', ['$http', 'Config', function($http, Config) {   
    var self = {};

    self.getAll = function (paramsObj) {
        return $http.post(Config.ApiUrl + 'Question/GetAll', paramsObj);
    };

    self.markAsRead = function (model) {
        return $http.post(Config.ApiUrl + 'Question/MarkAsRead', model);
    };

    self.sendAnswer = function (model) {
        return $http.post(Config.ApiUrl + 'Email/SendAnswer', model);
    };

    return self;
}]);