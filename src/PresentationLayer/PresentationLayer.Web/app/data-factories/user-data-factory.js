angular.module('factories').
factory('UserDataFactory', ['$http', 'Config', function($http, Config) {   
    var self = {};

    self.getAll = function (paramsObj) {
        return $http.post(Config.ApiUrl + 'User/GetAll', paramsObj);
    };

    self.insert = function (paramsObj) {
        return $http.post(Config.ApiUrl + 'User/Register', paramsObj);
    };

    return self;
}]);