﻿angular.module('factories').
factory('AuthenticationDataFactory', ['$http', 'Config', function($http, Config) {
    var self = {};



    self.logIn = function(data) {
        return $http.post(Config.ApiUrl + 'Account/LogIn', data);
    };

    self.logOut = function() {
        return $http.post(Config.ApiUrl + 'Account/LogOut');
    };
    
    self.isUserLogged = function() {
        return $http.get(Config.ApiUrl + 'Account/IsUserLogged');
    };

    return self;
}]);