angular.module('factories').
factory('ReceptionDataFactory', ['$http', 'Config', function($http, Config) {   
    var self = {};

    self.getAll = function (paramsObj) {
        return $http.post(Config.ApiUrl + 'Reception/GetAll', paramsObj);
    };

    self.markAsRead = function (model) {
        return $http.post(Config.ApiUrl + 'Reception/MarkAsRead', model);
    };

    return self;
}]);