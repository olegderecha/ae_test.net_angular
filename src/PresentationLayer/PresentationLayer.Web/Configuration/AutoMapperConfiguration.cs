﻿using AutoMapper;
using PresentationLayer.Web.Mappings;

namespace PresentationLayer.Web.Configuration {
    internal static class AutoMapperConfiguration
    {
        internal static void Configure()
        {
            Mapper.Initialize(c =>
            {
                c.AddProfile<GeneralMappings>();
            });
        }
    }
}