﻿namespace PresentationLayer.Web.Models
{
    public class EmailRequest
    {
        public int QuestionId { get; set; }
        public string MessageText { get; set; }
    }
}