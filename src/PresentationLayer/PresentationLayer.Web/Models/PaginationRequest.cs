﻿namespace PresentationLayer.Web.Models
{
    public class PaginationRequest
    {
        private int _page;

        public int Count { get; set; }
        public int Page {
            get {
                return _page == 0 ? 1 : _page;
            }
            set {
                _page = value;
            }
        }
        public string SortBy { get; set; }
        public string SortOrder { get; set; }
    }
}