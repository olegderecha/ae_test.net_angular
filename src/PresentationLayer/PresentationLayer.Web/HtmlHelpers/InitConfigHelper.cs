﻿using System.Web.Mvc;

namespace PresentationLayer.Web.HtmlHelpers {
    public static class InitConfigHelper {

        public static MvcHtmlString InitialConfiguration(this HtmlHelper html){

            var initialConfiguration = string.Format(
                @"window.Hospital=
                {{
                    initialConfiguration: {{
                        baseUrl: '{0}'
                    }}
                }}",
                new UrlHelper(html.ViewContext.RequestContext).Content("~"));

            return new MvcHtmlString(initialConfiguration);
        }
    }
}