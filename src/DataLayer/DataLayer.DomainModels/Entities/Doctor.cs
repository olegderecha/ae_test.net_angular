﻿namespace DataLayer.DomainModels.Entities {
    public class Doctor : BaseEntity {
        
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
