﻿namespace DataLayer.DomainModels.Entities {
    public class Comment : BaseEntity {

        public string FirstName { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public bool IsRead { get; set; }
        public bool IsPublished { get; set; }

    }
}
