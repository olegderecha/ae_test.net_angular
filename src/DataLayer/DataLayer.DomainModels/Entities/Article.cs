﻿namespace DataLayer.DomainModels.Entities {
    public class Article : BaseEntity
    {
        public string Title { get; set; }

        public string Text { get; set; }

        public bool IsPublished { get; set; }

        public bool IsDeleted { get; set; }
    }
}
