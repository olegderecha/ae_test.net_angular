﻿using System.Collections.Generic;

namespace DataLayer.DomainModels.Entities {
    public class DoctorType : BaseEntity {
        
        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<Doctor> Doctors { get; set; }
    }
}