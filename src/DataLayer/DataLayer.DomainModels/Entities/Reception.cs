﻿using System;

namespace DataLayer.DomainModels.Entities {
    public class Reception : BaseEntity {

        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public bool IsRead { get; set; }

        //public int DoctorTypeId { get; set; }
        //public DoctorType DoctorType { get; set; }

    }
}
