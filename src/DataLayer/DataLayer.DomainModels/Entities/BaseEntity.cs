﻿using System;
using DataLayer.DomainModels.Base;

namespace DataLayer.DomainModels.Entities {
    public class BaseEntity : IBaseEntity {
        
        public int Id { get; set; }
        
        public DateTime? CreatedAt { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? UpdatedById { get; set; }
        public bool IsHidden { get; set; }

        virtual  public User UserCreatedBy { get; set; }
        virtual public User UserUpdatedBy { get; set; }

    }
}
