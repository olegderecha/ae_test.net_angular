﻿using System;

namespace DataLayer.DomainModels.Base {
    public interface IBaseEntity {
        int Id { get; set; }

        DateTime? CreatedAt { get; set; }
        int? CreatedById { get; set; }
        DateTime? UpdatedAt { get; set; }
        int? UpdatedById { get; set; }
        bool IsHidden { get; set; }
    }
}