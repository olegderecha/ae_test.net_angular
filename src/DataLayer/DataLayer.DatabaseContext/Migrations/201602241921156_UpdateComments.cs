namespace DataLayer.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateComments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "IsRead", c => c.Boolean(nullable: false));
            AddColumn("dbo.Comments", "IsPublished", c => c.Boolean(nullable: false));
            AddColumn("dbo.Comments", "UserCreatedBy_Id", c => c.Int());
            AddColumn("dbo.Comments", "UserUpdatedBy_Id", c => c.Int());
            CreateIndex("dbo.Comments", "UserCreatedBy_Id");
            CreateIndex("dbo.Comments", "UserUpdatedBy_Id");
            AddForeignKey("dbo.Comments", "UserCreatedBy_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Comments", "UserUpdatedBy_Id", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "UserUpdatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Comments", "UserCreatedBy_Id", "dbo.Users");
            DropIndex("dbo.Comments", new[] { "UserUpdatedBy_Id" });
            DropIndex("dbo.Comments", new[] { "UserCreatedBy_Id" });
            DropColumn("dbo.Comments", "UserUpdatedBy_Id");
            DropColumn("dbo.Comments", "UserCreatedBy_Id");
            DropColumn("dbo.Comments", "IsPublished");
            DropColumn("dbo.Comments", "IsRead");
        }
    }
}
