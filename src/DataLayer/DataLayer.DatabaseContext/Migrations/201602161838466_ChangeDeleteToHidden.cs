namespace DataLayer.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDeleteToHidden : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "IsHidden", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "IsHidden", c => c.Boolean(nullable: false));
            AddColumn("dbo.Doctors", "IsHidden", c => c.Boolean(nullable: false));
            AddColumn("dbo.DoctorTypes", "IsHidden", c => c.Boolean(nullable: false));
            AddColumn("dbo.Patients", "IsHidden", c => c.Boolean(nullable: false));
            AddColumn("dbo.Questions", "IsHidden", c => c.Boolean(nullable: false));
            AddColumn("dbo.Receptions", "IsHidden", c => c.Boolean(nullable: false));
            DropColumn("dbo.Users", "IsDeleted");
            DropColumn("dbo.Doctors", "IsDeleted");
            DropColumn("dbo.DoctorTypes", "IsDeleted");
            DropColumn("dbo.Patients", "IsDeleted");
            DropColumn("dbo.Questions", "IsDeleted");
            DropColumn("dbo.Receptions", "IsDeleted");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Receptions", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Questions", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Patients", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.DoctorTypes", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Doctors", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "IsDeleted", c => c.Boolean(nullable: false));
            DropColumn("dbo.Receptions", "IsHidden");
            DropColumn("dbo.Questions", "IsHidden");
            DropColumn("dbo.Patients", "IsHidden");
            DropColumn("dbo.DoctorTypes", "IsHidden");
            DropColumn("dbo.Doctors", "IsHidden");
            DropColumn("dbo.Users", "IsHidden");
            DropColumn("dbo.Articles", "IsHidden");
        }
    }
}
