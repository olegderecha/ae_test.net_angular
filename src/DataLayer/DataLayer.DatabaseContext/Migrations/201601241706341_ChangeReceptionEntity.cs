namespace DataLayer.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeReceptionEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Doctors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedAt = c.DateTime(),
                        CreatedById = c.Int(),
                        UpdatedAt = c.DateTime(),
                        UpdatedById = c.Int(),
                        UserCreatedBy_Id = c.Int(),
                        UserUpdatedBy_Id = c.Int(),
                        DoctorType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserCreatedBy_Id)
                .ForeignKey("dbo.Users", t => t.UserUpdatedBy_Id)
                .ForeignKey("dbo.DoctorTypes", t => t.DoctorType_Id)
                .Index(t => t.UserCreatedBy_Id)
                .Index(t => t.UserUpdatedBy_Id)
                .Index(t => t.DoctorType_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Description = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        CreatedAt = c.DateTime(),
                        CreatedById = c.Int(),
                        UpdatedAt = c.DateTime(),
                        UpdatedById = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DoctorTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedAt = c.DateTime(),
                        CreatedById = c.Int(),
                        UpdatedAt = c.DateTime(),
                        UpdatedById = c.Int(),
                        UserCreatedBy_Id = c.Int(),
                        UserUpdatedBy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserCreatedBy_Id)
                .ForeignKey("dbo.Users", t => t.UserUpdatedBy_Id)
                .Index(t => t.UserCreatedBy_Id)
                .Index(t => t.UserUpdatedBy_Id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        CreatedAt = c.DateTime(),
                        CreatedById = c.Int(),
                        UpdatedAt = c.DateTime(),
                        UpdatedById = c.Int(),
                        UserCreatedBy_Id = c.Int(),
                        UserUpdatedBy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserCreatedBy_Id)
                .ForeignKey("dbo.Users", t => t.UserUpdatedBy_Id)
                .Index(t => t.UserCreatedBy_Id)
                .Index(t => t.UserUpdatedBy_Id);
            
            CreateTable(
                "dbo.Receptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(),
                        DoctorTypeId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        CreatedById = c.Int(),
                        UpdatedAt = c.DateTime(),
                        UpdatedById = c.Int(),
                        UserCreatedBy_Id = c.Int(),
                        UserUpdatedBy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DoctorTypes", t => t.DoctorTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserCreatedBy_Id)
                .ForeignKey("dbo.Users", t => t.UserUpdatedBy_Id)
                .Index(t => t.DoctorTypeId)
                .Index(t => t.UserCreatedBy_Id)
                .Index(t => t.UserUpdatedBy_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Receptions", "UserUpdatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Receptions", "UserCreatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Receptions", "DoctorTypeId", "dbo.DoctorTypes");
            DropForeignKey("dbo.Questions", "UserUpdatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Questions", "UserCreatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.DoctorTypes", "UserUpdatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.DoctorTypes", "UserCreatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Doctors", "DoctorType_Id", "dbo.DoctorTypes");
            DropForeignKey("dbo.Doctors", "UserUpdatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Doctors", "UserCreatedBy_Id", "dbo.Users");
            DropIndex("dbo.Receptions", new[] { "UserUpdatedBy_Id" });
            DropIndex("dbo.Receptions", new[] { "UserCreatedBy_Id" });
            DropIndex("dbo.Receptions", new[] { "DoctorTypeId" });
            DropIndex("dbo.Questions", new[] { "UserUpdatedBy_Id" });
            DropIndex("dbo.Questions", new[] { "UserCreatedBy_Id" });
            DropIndex("dbo.DoctorTypes", new[] { "UserUpdatedBy_Id" });
            DropIndex("dbo.DoctorTypes", new[] { "UserCreatedBy_Id" });
            DropIndex("dbo.Doctors", new[] { "DoctorType_Id" });
            DropIndex("dbo.Doctors", new[] { "UserUpdatedBy_Id" });
            DropIndex("dbo.Doctors", new[] { "UserCreatedBy_Id" });
            DropTable("dbo.Receptions");
            DropTable("dbo.Questions");
            DropTable("dbo.DoctorTypes");
            DropTable("dbo.Users");
            DropTable("dbo.Doctors");
        }
    }
}
