namespace DataLayer.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Password", c => c.String());
            AddColumn("dbo.Users", "PasswordSalt", c => c.String());
            AddColumn("dbo.Users", "LastLogin", c => c.DateTime());
            AddColumn("dbo.Users", "Token", c => c.String());
            AddColumn("dbo.Users", "TokenExpirationDate", c => c.DateTime());
            AddColumn("dbo.Users", "ResetPasswordCode", c => c.String());
            AddColumn("dbo.Users", "ResetPasswordCodeExpiration", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "ResetPasswordCodeExpiration");
            DropColumn("dbo.Users", "ResetPasswordCode");
            DropColumn("dbo.Users", "TokenExpirationDate");
            DropColumn("dbo.Users", "Token");
            DropColumn("dbo.Users", "LastLogin");
            DropColumn("dbo.Users", "PasswordSalt");
            DropColumn("dbo.Users", "Password");
        }
    }
}
