namespace DataLayer.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsReadAndIsAnswered : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "IsRead", c => c.Boolean(nullable: false));
            AddColumn("dbo.Questions", "IsAnswered", c => c.Boolean(nullable: false));
            AddColumn("dbo.Receptions", "IsRead", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Receptions", "IsRead");
            DropColumn("dbo.Questions", "IsAnswered");
            DropColumn("dbo.Questions", "IsRead");
        }
    }
}
