namespace DataLayer.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedDeleted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Doctors", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.DoctorTypes", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Patients", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Receptions", "IsDeleted", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Questions", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Questions", "IsDeleted", c => c.Boolean());
            DropColumn("dbo.Receptions", "IsDeleted");
            DropColumn("dbo.Patients", "IsDeleted");
            DropColumn("dbo.DoctorTypes", "IsDeleted");
            DropColumn("dbo.Doctors", "IsDeleted");
            DropColumn("dbo.Users", "IsDeleted");
        }
    }
}
