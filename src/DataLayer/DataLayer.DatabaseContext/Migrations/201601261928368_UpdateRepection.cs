namespace DataLayer.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateRepection : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Receptions", "DoctorTypeId", "dbo.DoctorTypes");
            DropIndex("dbo.Receptions", new[] { "DoctorTypeId" });
            DropColumn("dbo.Receptions", "DoctorTypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Receptions", "DoctorTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Receptions", "DoctorTypeId");
            AddForeignKey("dbo.Receptions", "DoctorTypeId", "dbo.DoctorTypes", "Id", cascadeDelete: true);
        }
    }
}
