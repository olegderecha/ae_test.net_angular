namespace DataLayer.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeletedAndArticleAndPatients : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Text = c.String(),
                        IsPublished = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        CreatedById = c.Int(),
                        UpdatedAt = c.DateTime(),
                        UpdatedById = c.Int(),
                        UserCreatedBy_Id = c.Int(),
                        UserUpdatedBy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserCreatedBy_Id)
                .ForeignKey("dbo.Users", t => t.UserUpdatedBy_Id)
                .Index(t => t.UserCreatedBy_Id)
                .Index(t => t.UserUpdatedBy_Id);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Description = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        CreatedAt = c.DateTime(),
                        CreatedById = c.Int(),
                        UpdatedAt = c.DateTime(),
                        UpdatedById = c.Int(),
                        UserCreatedBy_Id = c.Int(),
                        UserUpdatedBy_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserCreatedBy_Id)
                .ForeignKey("dbo.Users", t => t.UserUpdatedBy_Id)
                .Index(t => t.UserCreatedBy_Id)
                .Index(t => t.UserUpdatedBy_Id);
            
            AddColumn("dbo.Questions", "IsDeleted", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Patients", "UserUpdatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Patients", "UserCreatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Articles", "UserUpdatedBy_Id", "dbo.Users");
            DropForeignKey("dbo.Articles", "UserCreatedBy_Id", "dbo.Users");
            DropIndex("dbo.Patients", new[] { "UserUpdatedBy_Id" });
            DropIndex("dbo.Patients", new[] { "UserCreatedBy_Id" });
            DropIndex("dbo.Articles", new[] { "UserUpdatedBy_Id" });
            DropIndex("dbo.Articles", new[] { "UserCreatedBy_Id" });
            DropColumn("dbo.Questions", "IsDeleted");
            DropTable("dbo.Patients");
            DropTable("dbo.Articles");
        }
    }
}
