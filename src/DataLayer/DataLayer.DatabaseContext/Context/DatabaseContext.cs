﻿using System.Data.Entity;
using DataLayer.DomainModels.Entities;

namespace DataLayer.DatabaseContext.Context {
    public class HospitalDbContext : DbContext {

        public DbSet<User> Users { get; set; }
        public DbSet<DoctorType> DoctorType { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Reception> Receptions { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public HospitalDbContext(string connectionString) : base(connectionString) {}
        public HospitalDbContext() : this("Hospital") {
        }
    }
}