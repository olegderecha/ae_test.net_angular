﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLayer.DatabaseContext.Context;
using DataLayer.DomainModels.Entities;
using DataLayer.Repositories.Extensions;

namespace DataLayer.Repositories
{
    public class UserRepository
    {
        private readonly HospitalDbContext _context = new HospitalDbContext();

        public ICollection<User> GetAll(int page, int count, string sortBy, string sortOrder)
        {
            return _context.Users
                .Where(q => q.IsHidden == false)
                .OrderBy(sortBy, sortOrder)
                .Skip(count * (page - 1))
                .Take(count)
                .ToList();
        }

        public int CountAll()
        {
            return _context.Users.Count(q => q.IsHidden == false);
        }

        public User GetUser(string login, string passwordSalt)
        {
            return _context.Users.FirstOrDefault(t => t.Login == login &&
                                                 t.PasswordSalt == passwordSalt);
        }

        public bool AddUser(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
            return true;
        }

        public bool UpdateUser(User user)
        {
            _context.Users.Attach(user);
            var entry = _context.Entry(user);
            entry.State = EntityState.Modified;
            _context.SaveChanges();
            return true;
        }

        public User GetUserByToken(string token)
        {
            return _context.Users.FirstOrDefault(t => t.Token == token);
        }

        public User GetUserByEmail(string email)
        {
            return _context.Users.FirstOrDefault(t => t.Email == email);
        }

        public User GetUserByResetPasswordCode(string code)
        {
            return _context.Users.FirstOrDefault(t => t.ResetPasswordCode == code);
        }        

    }
}
