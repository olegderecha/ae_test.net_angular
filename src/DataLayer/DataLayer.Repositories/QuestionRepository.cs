﻿using System.Collections.Generic;
using System.Linq;
using DataLayer.DatabaseContext.Context;
using DataLayer.DomainModels.Entities;
using DataLayer.Repositories.Extensions;

namespace DataLayer.Repositories
{
    public class QuestionRepository
    {
        private readonly HospitalDbContext _context = new HospitalDbContext();

        public ICollection<Question> GetAll(int page, int count, string sortBy, string sortOrder)
        {
            return _context.Questions
                .Where(q => q.IsHidden == false)
                .OrderBy(sortBy, sortOrder)
                .Skip(count * (page - 1))
                .Take(count)
                .ToList();
        }

        public int CountAll()
        {
            return _context.Questions.Count(q => q.IsHidden == false);
        }

        public int CountNotRead()
        {
            return _context.Questions
                .Count(q => q.IsHidden == false && q.IsRead == false);
        }

    }
}
