﻿using System.Linq;
using DataLayer.DomainModels.Base;

namespace DataLayer.Repositories.Interfaces {
    public interface IRepository<T> where T : class, IBaseEntity
    {
        T GetById(int id);
        bool Insert(object entity);
        bool Insert(T entity);
        bool Update(object entity);
        bool Update(T entity);
        bool Delete(int id);
        IQueryable<T> Table { get; }
    }
}