﻿using System.Collections.Generic;
using BusinessLayer.Services.Base;
using BusinessLayer.ServiceModels;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.Services.Helpers;
using DataLayer.Repositories;

namespace BusinessLayer.Services
{
    public class NavigationService
    {
        private readonly ReceptionRepository _receptionRepository = new ReceptionRepository();
        private readonly QuestionRepository _questionRepository = new QuestionRepository();
        private readonly CommentRepository _commentRepository = new CommentRepository();

        public NavigationModel GetCount()
        {
            return new NavigationModel {
                RepectionsCount = _receptionRepository.CountNotRead(),
                QuestionsCount = _questionRepository.CountNotRead(),
                CommentsCount = _commentRepository.CountNotRead(),
            };
        }
    }
}