﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.ServiceModels;

namespace BusinessLayer.Services.Helpers
{
    static class PagingHelper
    {

        public static PaginationModel GetPagination(int page, int count, int size)
        {
            int pages = ((size - 1) / count) + 1;
            return new PaginationModel
            {
                Count = count,
                Page = page == 0 ? 1 : page,
                Pages = pages,
                Size = size
            };
        }

    }
}
