﻿using System;
using System.Collections.Generic;
using DataLayer.DomainModels.Entities;
using DataLayer.Repositories;
using Module.Crypt;

namespace BusinessLayer.Services {
    public class AccountService {
        private readonly UserRepository _userRepository = new UserRepository();

        public bool AreCredentialsCorrect(User user) {
            if (user.Login == null || user.Password == null) { return false; }

            var userDb = _userRepository.GetUser(user.Login, PassowordConversion.EncodePassword(user.Password, user.Login.ToLower()));
            if (userDb != null) {
                return true;
            }
            return false;
        }

        public string Authorize(User user) {
            if (user.Login == null || user.Password == null){ return null; }

            var userDb = _userRepository.GetUser(user.Login, PassowordConversion.EncodePassword(user.Password, user.Login.ToLower()));
            if (userDb != null) {
                userDb.Token = Token.GenerateTokenWithTimeStamp();
                userDb.TokenExpirationDate = DateTime.Now.AddMonths(1);
                _userRepository.UpdateUser(userDb);
                return userDb.Token;
            }
            return null;
        }



        public bool IsTokenActual(string token){
            var user = _userRepository.GetUserByToken(token);
            if (user == null || user.Token == null) return false;

            if (user.TokenExpirationDate > DateTime.Now) {
                return true;
            }
            return false;
        }

        public bool IsEmailExist(string email) {
            var user = _userRepository.GetUserByEmail(email);
            if (user == null) return false;
            return true;
        }

        // reset password
        public string SetResetPasswordCode(string email){
            var user = _userRepository.GetUserByEmail(email);
            if (user == null) return "";

            user.ResetPasswordCode = RandomGenerator.GenerateString(15);
            user.ResetPasswordCodeExpiration = DateTime.Now.AddHours(1);
            _userRepository.UpdateUser(user);

            return user.ResetPasswordCode;
        }

        public bool IsResetPasswordRequestActual(string code) {
            var user = _userRepository.GetUserByResetPasswordCode(code);
            if (user == null || user.ResetPasswordCodeExpiration < DateTime.Now) return false;
            return true;
        }

        public bool UpdatePassword(string code, string newPassword) {
            var user = _userRepository.GetUserByResetPasswordCode(code);
            if (user == null) return false;
            user.PasswordSalt = PassowordConversion.EncodePassword(newPassword, user.Login.ToLower());
            return _userRepository.UpdateUser(user);
        }
    }
}
