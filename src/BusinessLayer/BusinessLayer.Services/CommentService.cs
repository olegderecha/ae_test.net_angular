﻿using AutoMapper;
using System.Collections.Generic;
using BusinessLayer.Services.Base;
using BusinessLayer.ServiceModels;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.Services.Helpers;
using DataLayer.Repositories;

namespace BusinessLayer.Services
{
    public class CommentService
    {
        private readonly CommentRepository _commentRepository = new CommentRepository();
        private readonly BaseCrudService<CommentModel> _commentCrudService = new BaseCrudService<CommentModel>();

        public CommentsModel GetAll(int page, int count, string sortBy, string sortOrder)
        {
            return new CommentsModel {
                Comments = Mapper.Map<ICollection<CommentModel>>(_commentRepository.GetAll(page, count, sortBy, sortOrder)),
                Pagination = PagingHelper.GetPagination(page, count, _commentRepository.CountAll())
            };
        }

        public bool MarkAsRead(int id)
        {
            var comment = Mapper.Map<CommentModel>(_commentCrudService.GetById(id));
            comment.IsRead = true;
            return _commentCrudService.Update(comment);
        }

        public bool Publish(int id)
        {
            var comment = Mapper.Map<CommentModel>(_commentCrudService.GetById(id));
            comment.IsPublished = true;
            return _commentCrudService.Update(comment);
        }
    }
}