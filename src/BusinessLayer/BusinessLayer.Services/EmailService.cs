﻿using System.Configuration;
using BusinessLayer.Services.Base;
using BusinessLayer.ServiceModels;
using Module.Mail;

namespace BusinessLayer.Services {
    public class EmailService {
        private readonly BaseCrudService<QuestionModel> _questionCrudService = new BaseCrudService<QuestionModel>();
        private readonly MailManager _mailManager = new MailManager();

        public bool SendAnswerEmail(int questionId, string messageText) {
            var question = _questionCrudService.GetById(questionId);

            EmailModel email = new EmailModel {
                ToAddress = question.Email,
                Subject = question.Title,
                Body = messageText
            };

            var result = _mailManager.SendEmail(email);
            if (result) {
                question.IsAnswered = true;
                return _questionCrudService.Update(question);
            }

            return false;
        }
    }
}
