﻿using AutoMapper;
using System.Collections.Generic;
using BusinessLayer.Services.Base;
using BusinessLayer.ServiceModels;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.Services.Helpers;
using DataLayer.Repositories;

namespace BusinessLayer.Services
{
    public class QuestionService
    {
        private readonly QuestionRepository _questionRepository = new QuestionRepository();
        private readonly BaseCrudService<QuestionModel> _questionCrudService = new BaseCrudService<QuestionModel>();

        public QuestionsModel GetAll(int page, int count, string sortBy, string sortOrder)
        {
            return new QuestionsModel {
                Questions = Mapper.Map<ICollection<QuestionModel>>(_questionRepository.GetAll(page, count, sortBy, sortOrder)),
                Pagination = PagingHelper.GetPagination(page, count, _questionRepository.CountAll())
            };
        }

        public bool MarkAsRead(int id)
        {
            var question = Mapper.Map<QuestionModel>(_questionCrudService.GetById(id));
            question.IsRead = true;
            return _questionCrudService.Update(question);
        }
    }
}