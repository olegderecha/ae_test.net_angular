﻿using BusinessLayer.ServiceModels.Base;

namespace BusinessLayer.Services.Interfaces
{
    public interface ICrudService<T> where T : IBaseModel
    {
        T GetById(int id);
        bool Insert(T entity);
        bool Update(T entity);
        bool Delete(int id);
    }
}