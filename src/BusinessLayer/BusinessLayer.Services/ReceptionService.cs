﻿using AutoMapper;
using System.Collections.Generic;
using BusinessLayer.Services.Base;
using BusinessLayer.ServiceModels;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.Services.Helpers;
using DataLayer.Repositories;

namespace BusinessLayer.Services
{
    public class ReceptionService
    {
        private readonly ReceptionRepository _receptionRepository = new ReceptionRepository();
        private readonly BaseCrudService<ReceptionModel> _receptionCrudService = new BaseCrudService<ReceptionModel>();

        public ReceptionsModel GetAll(int page, int count, string sortBy, string sortOrder)
        {
            return new ReceptionsModel {
                Receptions = Mapper.Map<ICollection<ReceptionModel>>(_receptionRepository.GetAll(page, count, sortBy, sortOrder)),
                Pagination = PagingHelper.GetPagination(page, count, _receptionRepository.CountAll())
            };
        }

        public bool MarkAsRead(int id)
        {
            var reception = Mapper.Map<ReceptionModel>(_receptionCrudService.GetById(id));
            reception.IsRead = true;
            return _receptionCrudService.Update(reception);
        }
    }
}