﻿using System;
using System.Linq;
using System.Reflection;
using AutoMapper;
using BusinessLayer.ServiceModels.Base;
using BusinessLayer.Services.Interfaces;
using DataLayer.DomainModels.Base;
using DataLayer.Repositories.Interfaces;

namespace BusinessLayer.Services.Base
{
    public class BaseCrudService<T> : ICrudService<T> where T : IBaseModel {
        private readonly dynamic _crudRepository;
        private readonly Type _entityType;
        private readonly Type _repositoryEntityType;

        public BaseCrudService() {
            var entityName = GetEntityTypeName();
            _entityType = GetEntityType(entityName);
            _repositoryEntityType = GetRepositoryEntityType();
            _crudRepository = GetCrudRepositoryInstance();
        }
        

        public T GetById(int id) {
            return Mapper.Map<T>(_crudRepository.GetById(id));
        }

        public bool Insert(T entityModel) {
            entityModel.CreatedAt = DateTime.Now;
            return _crudRepository.Insert(Mapper.Map(entityModel, typeof(T), _entityType));
        }

        public bool Update(T entityModel) {
            entityModel.UpdatedAt = DateTime.Now;
            return _crudRepository.Update(Mapper.Map(entityModel, typeof(T), _entityType));
        }

        public bool Delete(int id) {
            return _crudRepository.Delete(id);
        }

        public bool Hide(int id)
        {
            var entity = (IBaseEntity)_crudRepository.GetById(id);
            entity.UpdatedAt = DateTime.Now;
            entity.IsHidden = true;
            _crudRepository.Update(entity);
            return true;
        }

        public bool Show(int id)
        {
            var entity = (IBaseEntity)_crudRepository.GetById(id);
            entity.UpdatedAt = DateTime.Now;
            entity.IsHidden = false;
            _crudRepository.Update(entity);
            return true;
        }


        private string GetEntityTypeName() {
            return typeof (T).Name.Replace("Model", "");
        }

        private Type GetEntityType(string entityName)
        {
            return Assembly.GetAssembly(typeof(IBaseEntity)).GetTypes().First(t => t.Name.Equals(entityName));
        }

        private Type GetRepositoryEntityType()
        {
            return Assembly.GetAssembly(typeof(IRepository<>))
                .GetTypes()
                .First(t => t.IsGenericType && t.IsClass && t.GetInterfaces().Any(i => i.Name == (typeof(IRepository<>)).Name));
        }

        private dynamic GetCrudRepositoryInstance()
        {
            Type repositoryInstance = _repositoryEntityType.MakeGenericType(new[] { _entityType });
            return Activator.CreateInstance(repositoryInstance);
        }

        
    }
}