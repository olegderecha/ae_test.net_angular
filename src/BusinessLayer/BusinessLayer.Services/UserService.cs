﻿using AutoMapper;
using System.Collections.Generic;
using BusinessLayer.Services.Base;
using BusinessLayer.ServiceModels;
using BusinessLayer.ServiceModels.Paging;
using BusinessLayer.Services.Helpers;
using DataLayer.Repositories;
using Module.Crypt;
using DataLayer.DomainModels.Entities;
using System;

namespace BusinessLayer.Services
{
    public class UserService
    {
        private readonly UserRepository _userRepository = new UserRepository();
        private readonly CrudRepository<User> _crudUserRepository = new CrudRepository<User>();
        private readonly BaseCrudService<UserModel> _userCrudService = new BaseCrudService<UserModel>();

        public UsersModel GetAll(int page, int count, string sortBy, string sortOrder)
        {
            return new UsersModel {
                Users = Mapper.Map<ICollection<UserModel>>(_userRepository.GetAll(page, count, sortBy, sortOrder)),
                Pagination = PagingHelper.GetPagination(page, count, _userRepository.CountAll())
            };
        }

        public bool Insert(UserModel userModel)
        {
            if (userModel.Login == null || userModel.Password == null) { return false; }
            userModel.CreatedAt = DateTime.Now;
            var user = Mapper.Map<User>(userModel);
            user.PasswordSalt = PassowordConversion.EncodePassword(userModel.Password, userModel.Login.ToLower());
            return _userRepository.AddUser(user);
        }

        public bool Update(UserModel userModel)
        {
            var user = _crudUserRepository.GetById(userModel.Id);
            Mapper.Map(userModel, user);
            user.UpdatedAt = DateTime.Now;
            if (userModel.Password != "******") {
                user.PasswordSalt = PassowordConversion.EncodePassword(userModel.Password, userModel.Login.ToLower());
            }
            return _crudUserRepository.Update(user);
        }
    }
}