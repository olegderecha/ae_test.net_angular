﻿namespace BusinessLayer.ServiceModels {
    public class PaginationModel {

        public int Count { get; set; }
        public int Page { get; set; }
        public int Pages { get; set; }
        public int Size { get; set; }
    }
}