﻿using BusinessLayer.ServiceModels.Base;

namespace BusinessLayer.ServiceModels {
    public class QuestionModel : BaseModel {

        public string Title { get; set; }
        public string Description { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsRead { get; set; }
        public bool IsAnswered { get; set; }

    }
}