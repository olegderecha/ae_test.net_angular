﻿using BusinessLayer.ServiceModels.Base;

namespace BusinessLayer.ServiceModels {
    public class CommentModel : BaseModel {

        public string FirstName { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public bool IsHidden { get; set; }
        public bool IsRead { get; set; }
        public bool IsPublished { get; set; }

    }
}