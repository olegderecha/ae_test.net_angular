﻿namespace BusinessLayer.ServiceModels {
    public class NavigationModel {

        public int RepectionsCount { get; set; }
        public int QuestionsCount { get; set; }
        public int CommentsCount { get; set; }

    }
}