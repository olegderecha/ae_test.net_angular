﻿using BusinessLayer.ServiceModels.Base;

namespace BusinessLayer.ServiceModels {
    public class UserModel : BaseModel {

        public string Login { get; set; }

        public string Password { get; set; }


        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Description { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }


        //public DateTime? LastLogin { get; set; }

        //public string Token { get; set; }

        //public DateTime? TokenExpirationDate { get; set; }


        //public string ResetPasswordCode { get; set; }

        //public DateTime? ResetPasswordCodeExpiration { get; set; }

    }
}