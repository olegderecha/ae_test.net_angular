﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ServiceModels.Paging {
    public class ReceptionsModel
    {
        public ICollection<ReceptionModel> Receptions { get; set; }
        public PaginationModel Pagination { get; set; }
    }
}
