﻿using System.Collections.Generic;

namespace BusinessLayer.ServiceModels.Paging {
    public class QuestionsModel {
        
        public ICollection<QuestionModel> Questions { get; set; }
        public PaginationModel Pagination { get; set; }

    }
}