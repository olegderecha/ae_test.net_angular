﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ServiceModels.Paging {
    public class UsersModel
    {
        public ICollection<UserModel> Users { get; set; }
        public PaginationModel Pagination { get; set; }
    }
}
