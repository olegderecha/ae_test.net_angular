﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ServiceModels.Paging {
    public class CommentsModel
    {
        public ICollection<CommentModel> Comments { get; set; }
        public PaginationModel Pagination { get; set; }
    }
}
