﻿namespace BusinessLayer.ServiceModels
{
    public class EmailModel
    {
        public string FromAddress { get; set; }

        public string ToAddress { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public dynamic Templete { get; set; }
    }
}