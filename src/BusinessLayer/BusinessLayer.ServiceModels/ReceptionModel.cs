﻿using System;
using BusinessLayer.ServiceModels.Base;

namespace BusinessLayer.ServiceModels {
    public class ReceptionModel : BaseModel {

        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public bool IsRead { get; set; }

        public int DoctorTypeId { get; set; }
        public DoctorTypeModel DoctorType { get; set; }

    }
}