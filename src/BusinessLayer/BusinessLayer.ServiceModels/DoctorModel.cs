﻿using BusinessLayer.ServiceModels.Base;

namespace BusinessLayer.ServiceModels {
    public class DoctorModel : BaseModel {

        public string Name { get; set; }
        public string Description { get; set; }

    }
}