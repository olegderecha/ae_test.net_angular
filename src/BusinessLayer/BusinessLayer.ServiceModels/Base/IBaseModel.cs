﻿using System;
namespace BusinessLayer.ServiceModels.Base
{
    public interface IBaseModel
    {
        int Id { get; set; }
        DateTime? CreatedAt { get; set; }
        DateTime? UpdatedAt { get; set; }
    }
}
