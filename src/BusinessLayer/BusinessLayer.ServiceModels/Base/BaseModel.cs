﻿using System;

namespace BusinessLayer.ServiceModels.Base {
    public class BaseModel : IBaseModel {

        public int Id { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        virtual public UserModel UserCreatedBy { get; set; }
        virtual public UserModel UserUpdatedBy { get; set; }
    }
}
