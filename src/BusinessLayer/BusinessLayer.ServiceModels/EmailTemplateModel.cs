﻿namespace BusinessLayer.ServiceModels
{
    public class EmailTemplateModel
    {
        public string Login { get; set; }

        public string Url { get; set; }
    }
}