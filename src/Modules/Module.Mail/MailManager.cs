﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using BusinessLayer.ServiceModels;

namespace Module.Mail {
    public class MailManager {

        public bool SendEmail(EmailModel email){

            var smtpClient = new SmtpClient{
                Host = ConfigurationManager.AppSettings["Mail:SmtpServer"],
                Port = Int32.Parse(ConfigurationManager.AppSettings["Mail:SmtpPort"]),
                EnableSsl = bool.Parse(ConfigurationManager.AppSettings["Mail:UseSSL"]),
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Mail:Login"],
                                                    ConfigurationManager.AppSettings["Mail:Password"])
            };  

            try {
                var message = new MailMessage();

                message.Subject = email.Subject;
                
                message.IsBodyHtml = true;
                message.Body = email.Body;

                message.From = new MailAddress(ConfigurationManager.AppSettings["Mail:Login"]);
                message.To.Add(email.ToAddress);

                smtpClient.Send(message);

                return true;
            }
            catch (SmtpException ex) {
                return false;
            }
        }
    }
}
