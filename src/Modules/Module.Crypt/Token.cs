﻿using System;
using System.Linq;

namespace Module.Crypt {
    public class Token {

        public static string GenerateToken() {
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray());            
        }

        public static string GenerateTokenWithTimeStamp() {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            return Convert.ToBase64String(time.Concat(key).ToArray());
        }

        public DateTime GetTimeFromToken(string token) {
            byte[] data = Convert.FromBase64String(token);
            return DateTime.FromBinary(BitConverter.ToInt64(data, 0));
        }
    }
}
