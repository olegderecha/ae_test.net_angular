﻿using System;
using System.Linq;

namespace Module.Crypt {
    public class RandomGenerator {

        public static string GenerateString(int length) {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random(DateTime.Now.Millisecond);
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }


    }
}
